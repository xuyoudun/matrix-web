const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HappyPack = require('happypack');
const os = require('os');
const config = require('../config');

const active = process.argv.find((r) => r.indexOf('--active') === -1);
const happyThreadPool = HappyPack.ThreadPool({size: os.cpus().length});

module.exports = {
  entry: ['babel-polyfill', path.join(__dirname, '../src/Index.js')],
  output: {
    publicPath: '/',
    path: path.join(__dirname, '../dist'),
    filename: '[name][hash:11].js'
  },
  module: {
    rules: [{
      test: /\.css$/,
      use: ['style-loader', 'css-loader', 'postcss-loader'],
      exclude: /node_modules/
    }, {
      test: /\.(png|jpg|svg|gif|jpeg)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 1000,
          outputPath: 'images'
        }
      },
      exclude: /node_modules/
    }, {
      test: /\.(scss|sass)$/,
      use: ['style-loader', 'css-loader', 'sass-loader'],
      exclude: /node_modules/
    }, {
      test: /\.less$/,
      use: [
        {
          loader: 'style-loader'
        },
        {
          loader: 'css-loader'
        },
        {
          loader: 'less-loader',
          options: {
            modifyVars: {
              'primary-color': '#61dafb', // antd的主题颜色设定
              'link-color': '#61dafb',
              'font-size-base': '12px',
              'padding-lg': '20px', // containers
              'padding-md': '12px', // small containers and buttons
              'padding-sm': '8px', // Form controls and items
              'padding-xs': '4px;'// small items
            },
            javascriptEnabled: true
          }
        }
      ]
    }, {
      test: /(\.jsx|\.js)$/,
      loader: 'happypack/loader?id=happyjs',
      exclude: /node_modules/
    }]
  },
  devtool: 'source-map',
  plugins: [
    new HappyPack({
      id: 'happyjs',
      loaders: [
        'babel-loader?cacheDirectory=true',
        'eslint-loader'
      ],
      threadPool: happyThreadPool,
      cache: true,
      verbose: true
    }),
    new HtmlWebpackPlugin({
      template: path.join(__dirname, '../src/index.template.html'),
      favicon: path.resolve(__dirname, '../favicon.ico')
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env.CONFIG': JSON.stringify(config[active])
    })
  ],
  resolve: {
    modules: [
      path.join(__dirname, '../node_modules')
    ],
    alias: {
      'Components': path.join(__dirname, '../src/Public/Components'),
      'Images': path.join(__dirname, '../src/Public/Images'),
      'Styles': path.join(__dirname, '../src/Public/Styles'),
      'AutoRouter': path.join(__dirname, '../src/Public/AutoRouter.js'),
      'Stores': path.join(__dirname, '../src/Stores'),
      'Views': path.join(__dirname, '../src/Views'),
      'Utils': path.join(__dirname, '../src/Utils'),
      'Axios': path.join(__dirname, '../src/Utils/axios.js'),
      'Config': path.join(__dirname, '../config.js')
    }
  }
};
