const merge = require('webpack-merge');
const common = require('./webpack.common');

module.exports = merge(common, {
  devServer: {
    contentBase: '../dist',
    port: '9090',
    inline: true,
    historyApiFallback: true,
    hot: true
  }
});
