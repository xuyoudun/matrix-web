const merge = require('webpack-merge');
const common = require('./webpack.common');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const path = require('path');
const PurifyCssWebpack = require('purifycss-webpack');
const ParallelUglifyPlugin = require('webpack-parallel-uglify-plugin');
const glob = require('glob');

module.exports = merge(common, {
  optimization: {
    splitChunks: {
      chunks: 'all',
      name: true,
      automaticNameDelimiter: '-',
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10  // 优先级，越小优先级越高
        },
        default: {  // 默认设置，可被重写
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true  // 如果本来已经把代码提取出来，则重用存在的而不是重新产生
        }
      }
    }
  },
  devtool: 'source-map',
  plugins: [
    new CleanWebpackPlugin(['dist'], {
      root: path.resolve(__dirname, '..'),
      dry: false//启用删除文件
    }),
    new PurifyCssWebpack({
      paths: glob.sync(path.join(__dirname, '../src/*html'))
    }),
    new ParallelUglifyPlugin({
      cacheDir: '.cache/',
      uglifyJS: {
        output: {
          comments: false
        },
        compress: {
          drop_console: true,
          pure_funcs: ['window.console.log']
        },
        warnings: false
      }
    })
  ]
});
