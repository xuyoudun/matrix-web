# css 遵循规范
[Bem规范](https://juejin.im/post/5ae6d3b3f265da0b7964a8e7)

# git提交规范
+ feat：新功能（feature）
+ fix：修补bug
+ docs：文档（documentation）
+ style： 格式（不影响代码运行的变动）
+ refactor：重构（即不是新增功能，也不是修改bug的代码变动）
+ test：增加测试
+ chore：构建过程或辅助工具的变动

# git分支规范
+ master主分支 正式环境分支
+ develop开发分支 测试环境分支 合并至master分支
+ feature功能分支 合并至develop分支
+ release版本分支 基于develop创建
+ hotfix热修复分支 基于master创建 修复线上问题

# 项目架构
+ src
    + Public
        + Components
            + LoginComponent
                + LoginComponent.js
                + LoginComponent.scss
        + Images
        + Styles
    + Stores
        + LoginStore
            + LoginStore.js
        + IndexStore
    + Utils
    + Views
        + LoginView
            + Login.js
            + Login.scss


