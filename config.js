module.exports = {

  // DEV环境
  dev: {
    server: 'https://api-gateway-dev.matrix.com',
    uploadURL: 'https://api-gateway-dev.matrix.com/upload',
    downloadURL: 'https://api-gateway-dev.matrix.com/download'
  },

  // UAT环境
  uat: {
    server: 'https://api-gateway-dev.matrix.com',
    uploadURL: 'https://api-gateway-dev.matrix.com/upload',
    downloadURL: 'https://api-gateway-dev.matrix.com/download'
  },

  // Production 环境
  prod: {
    server: 'https://api-gateway-dev.matrix.com',
    uploadURL: 'https://api-gateway-dev.matrix.com/upload',
    downloadURL: 'https://api-gateway-dev.matrix.com/download'
  }
};
