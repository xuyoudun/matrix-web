import React, {Component} from 'react';
import {Button, Form, Icon, Input} from 'antd';
import PropTypes from 'prop-types';
import './LoginView.scss';
import logo from '../../Public/Images/logo.png';
import LoginStore from 'Stores/LoginStore/LoginStore';
import IndexStore from '../../Stores/IndexStore';
import {getData, logout, setAccessToken} from '../../Utils/Common';

const FormItem = Form.Item;

class LoginView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      identifyCode: '1234'
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.refreshCode = this.refreshCode.bind(this);
    this.getMenuAuth = this.getMenuAuth.bind(this);
  }

  componentDidMount() {
    this.refreshCode();
    logout();
    const accessToken = getData(
        () => this.getQueryString('access_token').split('#')[0], null);
    const logoutUrl = getData(
        () => this.getQueryString('logout_url').split('#')[0], null);
    if (accessToken !== null) {
      localStorage.logoutUrl = logoutUrl;
      setAccessToken(`bearer ${accessToken}`, 25486);
      let user = {};
      this.getMenuAuth(user, true);
    }
  }

  getQueryString(name) {
    let reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    let href = window.location.href.replace('&amp;', '&');
    let r = href.substring(href.indexOf('?') + 1).match(reg);
    if (r != null) return unescape(r[2]);
    return null;
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const data = {
          username: values.username,
          password: values.password
        };
        LoginStore.axiosLogin(data).then((res) => {
          if (res) {
            // message.success('登录成功')
            LoginStore.setLoginData(res);
            // setCookie('token', res.access_token, res.expires_in)
            let token = `${res.token_type} ${res.access_token}`;
            let user = {};
            setAccessToken(token, res.expires_in);
            this.getMenuAuth(user);
          }
        }).catch((err) => {
          if (err) {
            window.console.log(err);
            // message.error('网络出错...');
          }
        });
      }
    });
  }

  getMenuAuth(user, ssoIf) {
    const {history} = this.props;
    LoginStore.axiosGetMenus().then((data) => {
      if (data) {
        if (data.menuList) {
          LoginStore.setMenusData(data.menuList);
        }
        if (data.positionList) {
          LoginStore.setPositionData(data.positionList);
        }
        // if (data.data.roleList) {
        //     LoginStore.setRoleData(data.data.roleList);
        // }
        // user.authList=authList;
        user.menuList = data.menuList;
        // user.roleList = data.data.roleList;
        user.position = data.positionList;
        user.authenData = LoginStore.getLoginData;
        user.username = data.loginName;
        user.realname = data.userName;
        // const currentPosition = data.data.position.find((position) =>
        //     position.code == data.currentPositionCode);
        user.currentPosition = data.currentPosition;
        // {
        //     code: data.currentPositionCode,
        //     officeName: currentPosition.name
        // };
        // Common.setAccessToken(res.access_token);
        IndexStore.setUserInfo(user);
        IndexStore.setRouters([]);
        // setAccessToken(token);
        if (ssoIf) {
          window.location.href = `https://${window.location.host}/#/workbench?name=我的工作台`;
        }else {
          history.push({pathname: '/', search: '?name=我的工作台'});
        }
      }
    });
  }

  refreshCode() {
    this.setState({
      identifyCode: ''
    });
  }

  render() {
    const {getFieldDecorator} = this.props.form;
    return (
        <div className="LoginView">
          {
            this.getQueryString('access_token') ? '' : (
                <Form className="LoginView_Form"
                    onSubmit={this.handleSubmit}
                >
                  <div style={{textAlign: 'center', width: 340, height: 80}}>
                    <img
                        src={logo}
                        style={{width: 147}}
                    />
                  </div>
                  <FormItem>
                    {getFieldDecorator('username', {
                      rules: [
                        {
                          required: true,
                          message: '用户名'
                        }]
                    })(
                        <Input placeholder="用户名"
                            prefix={<Icon type="user"/>}
                        />,
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator('password', {
                      rules: [
                        {
                          required: true,
                          message: '请输入密码'
                        }]
                    })(
                        <Input placeholder="密码"
                            prefix={<Icon type="lock"/>}
                            type="password"
                        />,
                    )}
                  </FormItem>
                  <FormItem>
                    <Button
                        block
                        htmlType="submit"
                        size="small"
                        type="primary"
                    > {'登陆'}
                    </Button>
                  </FormItem>
                </Form>
            )
          }
        </div>
    );
  }
}

LoginView.propTypes = {
  history: PropTypes.object,
  form: PropTypes.object
};
export default Form.create()(LoginView);
