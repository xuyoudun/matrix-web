import React, {Component} from 'react';
import {Button, Dropdown, Icon, Layout, Menu, Spin} from 'antd';
import {observer} from 'mobx-react';
import {withRouter} from 'react-router-dom';
import IndexStore, {ROUTER_TAB_WORKBENCH} from 'Stores/IndexStore.js';
import getMenuData from '../../Public/Menus';
import AutoRouter from 'AutoRouter';

import './MainView.scss';

const {Header, Sider, Content} = Layout;
const SubMenu = Menu.SubMenu;

@observer
class MainView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      //logoPath: allLogo,
      menuData: [],
      position: [],
      roleList: [],
      officeName: '',
      positionName: '',
      realname: '',
      openKeys: [],
      latestOpenKey: ''
    };
    this.toggle = this.toggle.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onOpenChange = this.onOpenChange.bind(this);
  }

  componentDidMount() {
    const menuData = this.renderNavMenu(getMenuData());
    // 展开菜单
    this.setState({
      menuData,
      //position,
      //realname,
      openKeys: localStorage.getItem('openKeys') ? JSON.parse(localStorage.getItem('openKeys')) : [],
      latestOpenKey: localStorage.getItem('latestOpenKey')
    });
  }

  renderNavMenu(menus) {
    return menus.filter((item) => item.name && !item.hideInMenu).map((item) => {
      // make dom
      const itemDom = this.renderSubMenuOrItem(item);
      return itemDom;
      //return this.checkPermissionItem(item.authority, ItemDom);
    }).filter((item) => item);
  }

  renderSubMenuOrItem(menu) {
    if (menu.children && menu.children.some((child) => child.name)) {
      const childrenItems = this.renderNavMenu(menu.children);
      return childrenItems.length > 0 ? (
          <SubMenu
              className="subMenuTitle"
              key={menu.code}
              title={<span><Icon type="folder"/><span>{menu.name}</span></span>}
          >
            {childrenItems}
          </SubMenu>
      ) : null;
    } else {
      const {history} = this.props;
      return (
          <Menu.Item
              className="navMaxClick"
              key={menu.path}
              style={{float: 'left', padding: '0'}}
          >
            <div
                onClick={() => {
                  const newName = encodeURI(menu.name);
                  history.push(
                      {pathname: menu.path, search: `?name=${newName}`});
                }}
            >
              <Icon type="folder"/>
              <span>{menu.name}</span>
            </div>
          </Menu.Item>
      );
    }
  }

  handleChange(value/*, e*/) {
    this.setState({positionName: value});
  }

  toggle() {
    let {openKeys} = this.state;
    if (!this.state.collapsed) {
      openKeys = [];
    } else {
      openKeys = localStorage.getItem('openKeys') ? JSON.parse(
          localStorage.getItem('openKeys')) : [];
    }
    this.setState({
      collapsed: !this.state.collapsed,
      openKeys
    });
    const {collapsed} = this.state;
    let logoPath;
    if (collapsed) {
      //logoPath = allLogo;
    } else {
      //logoPath = iconOnlyLogo;
    }
    this.setState({
      logoPath
    });
  }

  // 展开菜单
  onOpenChange(openKeys) {
    // 只展开当前父级菜单
    let menusCodes = getMenuData().map((menu) => menu.code);
    const latestOpenKey = openKeys.find(
        (key) => this.state.openKeys.indexOf(key) === -1);
    if (menusCodes.indexOf(latestOpenKey) === -1) {
      openKeys = openKeys.filter((item) => item != this.state.latestOpenKey);
      this.setState({latestOpenKey});
      this.setState({openKeys});
      localStorage.setItem('latestOpenKey', latestOpenKey ? latestOpenKey : '');
      localStorage.setItem('openKeys',
          openKeys ? JSON.stringify(openKeys) : []);
    } else {
      this.setState({latestOpenKey: ''});
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : []
      });
      localStorage.setItem('latestOpenKey', latestOpenKey ? latestOpenKey : '');
      localStorage.setItem('openKeys',
          latestOpenKey ? JSON.stringify([latestOpenKey]) : []);
    }
  }

  render() {
    const {logoPath, menuData, /*position, officeName, positionName,*/ realname, openKeys} = this.state;
    const {history} = this.props;
    const path = history.location.pathname;
    const menu = (
        <Menu onClick={() => this.exitLogin()}>
          <Menu.Item key="1">{'退出登陆'}</Menu.Item>
        </Menu>
    );
    return (
        <div id={'MainView'}>
          <Layout style={{
            // maxHeight: 'calc(100vh)'
            height: '100%'
          }}
          >
            <Sider
                className="mainSider"
                collapsed={this.state.collapsed}
                collapsible
                trigger={null}
                width={300}
            >
              <div className="logo">
                <img src={logoPath}/>
              </div>
              <div className="menu-wrap">
                <Menu
                    mode="inline"
                    onOpenChange={this.onOpenChange}
                    openKeys={openKeys}
                    selectedKeys={[path || ROUTER_TAB_WORKBENCH[0].url]}
                    style={{backgroundColor: '#fff'}}
                    theme="light"
                >
                  {menuData}
                </Menu>
              </div>
            </Sider>
            <Layout style={{
              display: 'flex',
              flexDirection: 'column'
            }}
            >
              <Header style={{
                background: '#fff', //'url(' + banner + ')',
                backgroundRepeat: 'no-repeat',
                backgroundSize: '100% 100%',
                height: 79,
                padding: 0,
                display: 'flex',
                alignItems: 'center',
                overflowY: 'hidden',
                justifyContent: 'space-between',
                borderBottom: '3px solid #61dafb'
              }}
              >
                <div style={{display: 'flex', flexFlow: 'column'}}>
                  <Icon
                      className="trigger"
                      onClick={this.toggle}
                      type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                  />
                  <div style={{
                    lineHeight: 'normal',
                    paddingLeft: '24px',
                    color: '#61dafb',
                    letterSpacing: '6px'
                  }}
                  >
                    {this.state.collapsed ? '展开' : '收起'}
                  </div>
                </div>
                <div style={{
                  color: '#666',
                  paddingRight: 50,
                  display: 'flex',
                  alignItems: 'center'
                }}
                >
                  {/*<span style={{whiteSpace: 'nowrap'}}>{'当前组织：'}</span>
                  <Input readOnly
                      style={{width: 'auto', marginRight: 20}}
                      value={officeName}
                  />
                  <span style={{whiteSpace: 'nowrap'}}>{'当前职责：'}</span>
                  <Select
                      onChange={this.handleChange}
                      style={{width: 'auto', minWidth: 150}}
                      value={positionName || undefined}
                  >
                    {position.map((data) => {
                      return (
                          <Option
                              key={data.code}
                              value={data.code}
                          >
                            {data.name}
                          </Option>);
                    })}
                  </Select>*/}
                  <Dropdown overlay={menu}>
                    <Button
                        ghost
                        size="small"
                        style={{
                          marginLeft: 20,
                          border: '1px solid #30b333',
                          color: '#61dafb'
                        }}
                        type="primary"
                    >
                      {realname}
                    </Button>
                  </Dropdown>

                </div>
              </Header>
              <Content className={'content_layout'}
                  style={{
                         background: '#fff',
                         flex: '1'
                       }}
              >
                {IndexStore.getLoadingIf && <Spin spinning
                    wrapperClassName={'spinClass'}
                                            >{' '}</Spin>}
                <AutoRouter/>
              </Content>
            </Layout>
          </Layout>
          {/*<Progress/>*/}
        </div>
    );
  }
}


export default withRouter(MainView);
