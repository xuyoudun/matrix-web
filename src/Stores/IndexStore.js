import {action, computed, observable, toJS} from 'mobx';
import _ from 'lodash';

const ROUTER_TAB_WORKBENCH = {
  url: '/workbench',
  title: '我的工作台',
  search: '?name=我的工作台'
};

export {ROUTER_TAB_WORKBENCH};

class IndexStore {
  @observable data = 1;
  @observable isAuthenticated;
  @observable userInfo;
  @observable menusList;
  @observable updatePWD;
  @observable routers = [ROUTER_TAB_WORKBENCH];
  @observable loadingIf = false;
  @observable confirmFlag = false;


  constructor(isAuthenticated = false, updatePWD = 'N') {
    this.isAuthenticated = isAuthenticated;
    this.userInfo = {};
    this.menusList = [];
    this.updatePWD = updatePWD;
    this.routers = JSON.parse(sessionStorage.getItem('routers')) ||
        [ROUTER_TAB_WORKBENCH];
  }


  @computed get getConfirmFlag() {
    return this.confirmFlag;
  }

  @action setConfirmFlag(flag) {
    this.confirmFlag = flag;
  }

  @computed get getLoadingIf() {
    return this.loadingIf;
  }

  @action setLoadingIf(data) {
    this.loadingIf = data;
  }

  @computed get getRouters() {
    return toJS(this.routers);
  }

  @action setRouters(data) {
    sessionStorage.setItem('routers', JSON.stringify(data));
    this.routers = data;
  }

  @action removeRouter(pathname) {
    const data = this.getRouters;
    const index = data.findIndex((d) => d.url == pathname);
    data.splice(index, 1);
    this.setRouters(data);
  }

  @computed get getData() {
    return this.data;
  }

  @action setData() {
    this.data += 1;
  }

  @computed get updatePWDFlag() {
    return this.updatePWD;
  }

  @action setPWDFlag(flag = 'N') {
    this.updatePWD = flag;
  }

  @computed get isAuth() {
    return this.isAuthenticated;
  }

  @action setAuthenticated(flag) {
    this.isAuthenticated = flag;
  }

  @computed get getUserInfo() {
    let userInfo = localStorage.getItem('userInfo');
    if (userInfo) {
      userInfo = JSON.parse(userInfo);
    } else {
      userInfo = {userInfo: '', userName: ''};
    }
    return userInfo;
  }

  // 获取当前组织编码
  @computed get getCurrentOrgCode() {
    return _.get(this.getUserInfo, 'currentPosition.officeCode', null);
  }

  // 获取当前大区编码
  @computed get getCurrentRegionCode() {
    return _.get(this.getUserInfo, 'currentPosition.regionCode', null);
  }

  // 获取当前用户编码
  @computed get getCurrentUserCode() {
    return _.get(this.getUserInfo, 'username', null);
  }

  @action setUserInfo(user = {userId: '', userName: ''}) {
    localStorage.setItem('userInfo', JSON.stringify(user));
  }

  @computed get getMenusList() {
    return this.menusList;
  }

  @action setMenusList(value) {
    this.menusList = value;
  }
}

const indexStore = new IndexStore();

export default indexStore;
