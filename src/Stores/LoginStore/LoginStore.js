import axios from 'Axios';
import {observable, action, computed, toJS} from 'mobx';

class LoginStore {
  @observable loginData = {};
  @observable menusData = {}; // 菜单列表
  @observable positionData = {}; // 职位数据
  @observable roteData = {}; // 角色数据
  @observable authority = [];// 权限

  @computed get getLoginData() {
    return toJS(this.loginData);
  }

  @computed get getMenusData() {
    return toJS(this.menusData);
  }

  @computed get getPositionData() {
    return toJS(this.positionData);
  }

  @computed get getRoteData() {
    return toJS(this.roteData);
  }

  @computed get getAuthority() {
    return toJS(this.authority);
  }

  @action setLoginData(data) {
    this.loginData = data;
  }

  @action setMenusData(data) {
    this.menusData = data;
  }

  @action setPositionData(data) {
    this.positionData = data;
  }

  @action setRoleData(data) {
    this.roteData = data;
  }

  @action setAuthority(data) {
    this.authority = data;
  }

  axiosLogin(data) {
    return axios.post(
        `/oauth/oauth/token?username=${data.username}&password=${data.password}&grant_type=password`);
  }

  // 获取菜单
  axiosGetMenus() {
    return axios.get('/srm-common-service/user');
  }
}

const loginStore = new LoginStore();
export default loginStore;
