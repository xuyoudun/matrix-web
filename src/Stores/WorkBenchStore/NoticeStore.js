import {action, computed, observable, toJS} from 'mobx';

class NoticeStore {
  @observable dataList = []
  @observable waitingCount = 0;
  @observable waitedCount = 0;
  @observable rendingCount = 0;
  @observable rendedCount = 0;

  @computed get getWaitingCount() {
    return this.waitingCount;
  }

  @action setWaitingCount(data) {
    this.waitingCount = data;
  }

  @computed get getWaitedCount() {
    return this.waitedCount;
  }

  @action setWaitedCount(data) {
    this.waitedCount = data;
  }

  @computed get getReadingCount() {
    return this.rendingCount;
  }

  @action setReadingCount(data) {
    this.rendingCount = data;
  }

  @computed get getReadedCount() {
    return this.rendedCount;
  }

  @action setReadedCount(data) {
    this.rendedCount = data;
  }

  @computed get getDataList() {
    return toJS(this.dataList);
  }

  @action setDataList(data) {
    this.dataList = data;
  }

}

const noticeStore = new NoticeStore();
export default noticeStore;
