import React, {Component} from 'react';
import {Pagination} from 'antd';
import PropTypes from 'prop-types';

class MatrixPagination extends Component {

  constructor(props) {
    super(props);
    this.onShowSizeChange = this.onShowSizeChange.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onShowSizeChange(current, pageSize) {
    const {handleChange} = this.props;
    handleChange(current, pageSize);
  }

  onChange(page, pageSize) {
    const {handleChange} = this.props;
    handleChange(page, pageSize);
  }

  render() {
    const {
      current, total, pageSize,
      hideOnSinglePage = true,
      showQuickJumper = true,
      showSizeChanger = true,
      style = {marginRight: 12, marginTop: 12, float: 'right'},
      ...rest
    } = this.props;
    return total ? (
        <div style={{overflow: 'hidden'}}>
          <Pagination
              {...rest}
              current={current}
              hideOnSinglePage={hideOnSinglePage}
              onChange={this.onChange}
              onShowSizeChange={this.onShowSizeChange}
              pageSize={pageSize}
              showQuickJumper={showQuickJumper}
              showSizeChanger={showSizeChanger}
              showTotal={(totalElements) => { return `共 ${totalElements} 条`; }}
              style={style}
              total={total}
          />
        </div>
    ) : null;
  }
}

MatrixPagination.propTypes = {
  current: PropTypes.number,
  total: PropTypes.number,
  handleChange: PropTypes.func,
  pageSize: PropTypes.number,
  style: PropTypes.object
};

export default MatrixPagination;
