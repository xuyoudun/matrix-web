import React, {Component} from 'react';
import {Select} from 'antd';
import _ from 'lodash';

/**
 *
 * <p>Description:
 *  覆盖了下拉选择框的行为
 *  默认情况下 通过表单收集值 单选只返回code，多选返回[code...]
 *  覆盖后 单选返回{code:'',name:''} 多选返回[{code:'',name:''} ...]</p>
 * <p>Copyright: Copyright (c) 2019</p>
 * @author xuyoudun
 * @email udun.xu@hotmail.com
 * @date 2019年06月20日
 * @version 1.0
 */
function getCodesFromValue(props) {
  const {code = 'code', mode, value} = props;
  if (value && !_.isArray(value) && !_.isObject(value)) {
    throw '你需要传递一个对象或者数组作为value';
  }
  return {
    codes: value ? (mode === 'multiple'
        ? value.map((v) => v[code])
        : value[code]) : null
  };
}

class MatrixSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...getCodesFromValue(props)
    };
    this.onChange = this.onChange.bind(this);
  }

  static getDerivedStateFromProps(nextProps) {
    // Should be a controlled component.
    if ('value' in nextProps) {
      return {
        ...getCodesFromValue(nextProps)
      };
    }
    return null;
  }

  onChange(value, option) {
    const {name = 'name', code = 'code', mode} = this.props;
    this.setState({codes: value});
    const {onChange} = this.props;
    if (option) {
      const values = (mode === 'multiple' ?
          option.map((op) => {
            const {value, children} = op.props;
            return {[code]: value, [name]: children};
          }) : {
            [code]: option.props.value,
            [name]: option.props.children
          });
      onChange && onChange(values, option);
    } else {
      onChange && onChange(undefined, option);
    }
  }

  render() {
    const {children} = this.props;
    return (
        <Select {...this.props}
            onChange={this.onChange}
            value={this.state.codes}
        >{children}</Select>);
  }
}

export default MatrixSelect;

// 拆包,表单收集的数据可能不符合传递到后台的参数的要求，这不可避免，需要一些额外的处理
export function unpacks(values, rule/*{key:(value)=>{handle value and return new object merge to origin}}*/) {
  const unpack = (obj) => {
    const origin = {};
    Object.entries(obj).forEach(([key, value]) => {
      const handle = rule[key];
      if (handle) {
        _.isFunction(handle) && Object.assign(origin, handle(value));
      } else {
        origin[key] = value;
      }
    });
    return origin;
  };
  return _.isArray(values) ? values.map(unpack) : unpack(values);
}
