import React, {Component} from 'react';
import {Input} from 'antd';

/**
 *
 * <p>Description:
 *  弹窗选择框
 */
class MatrixPopupSelect extends Component {

  constructor(props) {
    super(props);
    this.handleSearch = this.handleSearch.bind(this);
  }

  handleSearch(value) {
    window.console.log(value);
  }

  render() {
    return (
        <Input.Search enterButton
            onSearch={this.handleSearch}
            placeholder="input search text"
        />
    );
  }
}

export default MatrixPopupSelect;
