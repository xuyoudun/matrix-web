/**
 *
 * <p>Description: 导航路由标签页</p>
 * <p>Copyright: Copyright (c) 2019</p>
 * @author xuyoudun
 * @email udun.xu@hotmail.com
 * @date 2019年05月20日
 * @version 1.0utils.js
 repeat.js
 */
import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {withRouter} from 'react-router-dom';
import {Dropdown, Icon, Menu, Tabs} from 'antd';
import {dropByCacheKey, getCachingKeys} from 'react-router-cache-route';
import IndexStore, {ROUTER_TAB_WORKBENCH} from 'Stores/IndexStore.js';
import _ from 'lodash';
import PubSub from 'pubsub-js';
import {getRequest} from 'Utils/utils';

const TabPane = Tabs.TabPane;

function ensureNoSlash(pathname) {
  //由于location的pathname有时会有/后缀有时没有 这里统一
  return _.last(pathname) == '/'
      ? pathname.slice(0, pathname.length - 1)
      : pathname;
}

@observer
class RouterTabs extends Component {
  static active = 0;

  constructor(props) {
    super(props);
    this.state = {
      routers: []
    };
    this.handleChangeTabs = this.handleChangeTabs.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.remove = this.remove.bind(this);
    this.handleDeleteAll = this.handleDeleteAll.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleDeleteOthers = this.handleDeleteOthers.bind(this);
  }

  static getDerivedStateFromProps(nextProps) {
    const {location} = nextProps;
    //如果跳转到首页/不需要设置在tab数据中
    if (location.pathname !== '/') {
      const origin = IndexStore.getRouters;
      //由于location的pathname有时会有/后缀有时没有 这里统一
      const pathname = ensureNoSlash(location.pathname);
      const item = origin.find((o) => o.url == pathname);
      const index = origin.findIndex((o) => o.url == pathname);
      //如果store存储的没有当前路由
      if (item == undefined) {
        //则向store中添加一条新路由tab
        //路由item为 一个url为路由 title为name值主要用于tab的name search为?的查询字符串
        const title = decodeURI(getRequest('name'));
        //在激活页右侧插入
        origin.splice(++RouterTabs.active, 0, {
          url: pathname,
          title,
          search: location.search
        });
        IndexStore.setRouters(origin);
      }else {
        RouterTabs.active = index;
        if (item.search !== location.search) {
          //如果store中有当前路由，但是查询参数不同
          //则找到store的那一条 更改那一条的search
          origin[index] = {
            url: item.url,
            title: item.title,
            search: location.search
          };
          IndexStore.setRouters(origin);
        }
      }
    }
    return null;
  }

  componentDidMount() {
    PubSub.subscribe('removeTab', (msg, data) => {
      this.remove(data);
    });
  }

  handleChangeTabs(key) {
    const {history} = this.props;
    const data = IndexStore.getRouters;
    const item = data.find((d) => d.url == key);
    history.push({pathname: key, search: item.search});
  }

  onEdit(targetKey, action) {
    this[action](targetKey);
  }

  remove(key) {
    dropByCacheKey(key);
    const {location} = this.props;
    const data = IndexStore.getRouters;
    const index = data.findIndex((d) => d.url == key);
    const pathname = ensureNoSlash(location.pathname);
    data.splice(index, 1);
    IndexStore.setRouters(data);
    // 移除当前激活的tab页，则跳转到前一个tab页
    if (key === pathname) {
      const last = data[index - 1];
      const {history} = this.props;
      history.push({pathname: last.url, search: last.search});
    }
  }

  handleDeleteAll() {
    getCachingKeys().
        filter((r) => r != ROUTER_TAB_WORKBENCH.url).
        forEach((r) => dropByCacheKey(r));
    IndexStore.setRouters([ROUTER_TAB_WORKBENCH]);
    const {history} = this.props;
    history.push({
      pathname: ROUTER_TAB_WORKBENCH.url,
      search: ROUTER_TAB_WORKBENCH.search
    });
  }

  handleDeleteOthers(name) {
    getCachingKeys().
        filter((r) => r != ROUTER_TAB_WORKBENCH.url && r != name).
        forEach((r) => dropByCacheKey(r));
    const data = [ROUTER_TAB_WORKBENCH];//copy
    const origin = IndexStore.getRouters;
    const item = origin.find((o) => o.url == name);
    if (name !== ROUTER_TAB_WORKBENCH.url) {
      data.push(item);
    }
    IndexStore.setRouters(data);
    const {history} = this.props;
    history.push({pathname: item.url, search: item.search});
  }

  handleClick(name, {key}) {
    switch (key) {
      case 'current':
        this.remove(name);
        break;
      case 'others':
        this.handleDeleteOthers(name);
        break;
      case 'all':
        this.handleDeleteAll();
        break;
    }
  }

  render() {
    const {location} = this.props;
    const pathname = ensureNoSlash(location.pathname);
    const menu = (
        <Menu onClick={(e) => this.handleClick(pathname, e)}>
          <Menu.Item disabled={pathname == ROUTER_TAB_WORKBENCH.url}
              key="current"
          >{'关闭当前'}</Menu.Item>
          <Menu.Item key="others">{'关闭其他'}</Menu.Item>
          <Menu.Item key="all">{'全部关闭'}</Menu.Item>
        </Menu>
    );
    const menuDelete = (
        <Dropdown overlay={menu}
            placement="bottomCenter"
        >
          <div style={{
            color: '#fff',
            background: '#61dafb',
            padding: '0 6px',
            cursor: 'pointer'
          }}
          >{'页签操作'}
            <Icon style={{paddingLeft: '2px'}}
                type="down"
            /></div>
        </Dropdown>
    );
    return (
        <Tabs
            activeKey={pathname}
            animated={false}
            hideAdd
            onChange={this.handleChangeTabs}
            onEdit={this.onEdit}
            tabBarExtraContent={IndexStore.getRouters.length == 1
                ? ''
                : menuDelete}
            type="editable-card"
        >
          {
            IndexStore.getRouters.map((r) => (
                <TabPane
                    closable={r.url != ROUTER_TAB_WORKBENCH.url}
                    key={r.url}
                    tab={r.title}
                />
            ))
          }
        </Tabs>
    );
  }
}

export default withRouter(RouterTabs);
