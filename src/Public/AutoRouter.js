/*eslint-disable no-use-before-define*/
/*eslint-disable react/display-name*/
/*eslint-disable react/no-multi-comp*/
import React, {Component, createElement} from 'react';
import {withRouter} from 'react-router-dom';
import {Spin} from 'antd';
import CacheRoute, {CacheSwitch} from 'react-router-cache-route';
import RouterTabs from './Components/RouterTabs/RouterTabs';
import Loadable from 'react-loadable';
import {findMenuByRouter} from './Menus';


// 全局路由配置列表
const routerConfig = {
  '/workbench': {
    component: dynamicWrapper(() => import('../Views/WorkBenchView/WorkBenchView'))
  }
};

const routerData = getRouterData();

function getRouterData() {
  // Route configuration data
  // eg. {name,authority ...routerConfig }
  const routerData = {};
  // The route matches the menu
  Object.keys(routerConfig).forEach((path) => {
    // Regular match item name
    // eg.  router /user/:id === /user/chen
    const menuItem = findMenuByRouter(path) || {};
    let router = routerConfig[path];
    // If you need to configure complex parameter routing,
    // eg . /list/:type/user/info/:id
    router = {
      ...router,
      name: router.name || menuItem.name,
      authority: router.authority || menuItem.authority,
      hideInBreadcrumb: router.hideInBreadcrumb || menuItem.hideInBreadcrumb/*,
      inherited*/
    };
    routerData[path] = router;
  });
  return routerData;
}

function dynamicWrapper(component) {
  return Loadable({
    loader: () => {
      return component().then((raw) => {
        const Component = withRouter(raw.default || raw);
        return (props) =>
            createElement(Component, {
              ...props,
              getNestedRoutes: () => getRoutes(props.match.path, routerData)
            });
      });
    },
    loading: () => (
        <Spin className="global-spin"
            size="small"
        />)
  });
}

function getRelation(str1, str2) {
  if (str1 === str2) {
    console.warn('Two path are equal!'); // eslint-disable-line
  }
  const arr1 = str1.split('/');
  const arr2 = str2.split('/');
  if (arr2.every((item, index) => item === arr1[index])) {
    return 1;
  } else if (arr1.every((item, index) => item === arr2[index])) {
    return 2;
  }
  return 3;
}

function getRenderArr(routes) {

  let renderArr = [];
  renderArr.push(routes[0]);
  for (let i = 1; i < routes.length; i += 1) {
    // 去重
    renderArr = renderArr.filter((item) => getRelation(item, routes[i]) !== 1);
    // 是否包含
    const isAdd = renderArr.every((item) => getRelation(item, routes[i]) === 3);
    if (isAdd) {
      renderArr.push(routes[i]);
    }
  }
  return renderArr;
}

/**
 * Get router routing configuration
 * { path:{name,...param}}=>Array<{name,path ...param}>
 * @param {string} path
 * @param {routerData} routerData
 */
export function getRoutes(path, routerData) {
  let routes = Object.keys(routerData).filter(
      (routePath) => routePath.indexOf(path) === 0 && routePath !== path
  );
  // Replace path to '' eg. path='user' /user/name => name
  routes = routes.map((item) => item.replace(path, ''));
  // Get the route to be rendered to remove the deep rendering
  const renderArr = getRenderArr(routes);
  // Conversion and stitching parameters
  const renderRoutes = renderArr.map((item) => {
    const exact = !routes.some((route) => route !== item && getRelation(route, item) === 1);
    return {
      exact,
      ...routerData[`${path}${item}`],
      key: `${path}${item}`,
      path: `${path}${item}`
    };
  });
  return renderRoutes;
}

class AutoRouter extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    const {
      match
    } = this.props;

    return (
        <div
            className="autoRouterContainer"
            style={{
              flex: 1,
              display: 'flex',
              flexDirection: 'column',
              padding: '12px 12px'
            }}
        >
          <RouterTabs/>
          <CacheSwitch>
            {getRoutes(match.path, routerData).map((item) =>
                (
                    <CacheRoute
                        cacheKey={item.key}
                        component={item.component}
                        exact={item.exact}
                        key={item.key}
                        path={item.path}
                    />
                )
            )}
          </CacheSwitch>
        </div>
    );
  }

}

export default withRouter(AutoRouter);
