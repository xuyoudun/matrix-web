import pathToRegexp from 'path-to-regexp';

// 本地菜单
const menuData = [
  {
    code: 'WORKBENCH',
    name: '我的工作台',
    path: '/workbench',
    icon: 'folder'
  },
  //  空间管理
  {
    code: 'SPACE_MANAGE',
    name: '空间管理',
    icon: 'folder',
    children: [

      {
        code: 'BUILDING_MANAGE',
        name: '楼栋管理',
        icon: 'folder',
        path: '/space/buildingSummary'
      },
      {
        code: 'FLOOR_MANAGE',
        name: '楼层管理',
        icon: 'folder',
        path: '/space/floorSummary'
      },
      {
        code: 'UNIT_MANAGE',
        name: '单元管理',
        icon: 'folder',
        path: '/space/unitSummary'
      },
      {
        code: 'INTEGRATED_MANAGE',
        name: '空间综合管理',
        icon: 'folder',
        path: '/space/integratedManagement'
      }
    ]
  },

  // 客户管理
  {
    code: 'CUSTOMER_MANAGE',
    name: '客户管理',
    icon: 'folder',
    children: [
      {
        code: 'CUSTOMER_LIST',
        name: '客户汇总界面',
        icon: 'folder',
        path: '/customer/customerList'
      }
    ]
  },

  // 中介管理
  {
    code: 'INTERMEDIARY_MANAGE',
    name: '中介管理',
    icon: 'folder',
    children: [
      {
        code: 'INTERMEDIARY_LIST',
        name: '中介汇总界面',
        icon: 'folder',
        path: '/intermediary/intermediaryList'
      }
    ]
  },

  // 合同管理
  {
    code: 'CONTRACT_MANAGE',
    name: '合同管理',
    icon: 'folder',
    children: [
      {
        code: 'SETTLEMENT_TERMS',
        name: '结算条款库',
        icon: 'folder',
        path: '/contract/settlementClause'
      },
      {
        code: 'LEASE_CONFIRMATION',
        name: '租赁条件确认单',
        icon: 'folder',
        path: '/contract/leaseListView'
      },
      {
        code: 'CONTRACT_CLASSIFY_MANAGE',
        name: '合同',
        icon: 'folder',
        children: [
          {
            code: 'OFFICE_CONTRACT',
            name: '写字楼合同',
            icon: 'folder',
            path: '/contract/officeBuilding'
          },
          {
            code: 'BUSINESS_CONTRACT',
            name: '商业合同',
            icon: 'folder',
            path: '/contract/business'
          },
          {
            code: 'FACTORY_CONTRACT',
            name: '厂房合同',
            icon: 'folder',
            path: '/contract/factory'
          },
          {
            code: 'OTHERS_CONTRACT',
            name: '其他业务合同',
            icon: 'folder',
            path: '/contract/others'
          },
          {
            code: 'FICTITIOUS_CONTRACT',
            name: '虚拟合同',
            icon: 'folder',
            path: '/contract/fictitious'
          }
        ]
      },
      {
        code: 'COMMISSION_TERMS_DEPOSITORY',
        name: '佣金条款库',
        icon: 'folder',
        path: '/contract/commissionTermsSummary'
      }
    ]
  },

  // 结算菜单
  {
    code: 'SETTLEMENT_MANAGE',
    name: '结算管理',
    icon: 'folder',
    children: [
      {
        code: 'SETTLEMENT_REQUEST',
        name: '结算请求',
        icon: 'folder',
        path: '/settlement/settlementRequest'
      },
      {
        code: 'RECEIVABLE_MANAGE',
        name: '应收管理',
        icon: 'folder',
        path: '/settlement/receivableManage'
      },
      {
        code: 'SUNDRY_BILL_MANAGE',
        name: '杂项单据管理',
        icon: 'folder',
        path: '/settlement/sundryBillManage'
      },
      {
        code: 'BREACH_MANAGE',
        name: '违约金管理',
        icon: 'folder',
        path: '/settlement/breackPromiseManage'
      },
      {
        code: 'SALE_VOLUME_MANAGE',
        name: '销售额管理',
        icon: 'folder',
        path: '/settlement/saleVolumeManage'
      },
      {
        code: 'DEPOSIT_MANAGE',
        name: '定押金管理',
        icon: 'folder',
        path: '/settlement/depositManage'
      },
      {
        code: 'INVOICE_MANAGE',
        name: '发单管理',
        icon: 'folder',
        path: '/settlement/invoiceManage'
      },
      {
        code: 'RECEIPT_MANAGE',
        name: '收款管理',
        icon: 'folder',
        path: '/settlement/receiptManage'
      },
      {
        code: 'SEVICE_FEE_MANAGE',
        name: '服务费用管理',
        icon: 'folder',
        path: '/settlement/SeviceFeeManage'
      },
      {
        code: 'ENERGY_FEE_MANAGE',
        name: '能耗费用管理',
        icon: 'folder',
        children: [
          {
            code: 'ENERGY_DATA_MANAGE',
            name: '能耗数据管理',
            icon: 'folder',
            path: '/settlement/energyDataManage'
          },
          {
            code: 'ENERGY_METER_MANAGE',
            name: '能耗表管理',
            icon: 'folder',
            path: '/settlement/energyTableManage'
          },
          {
            code: 'SPLIT_RULES_MANAGE',
            name: '拆分规则管理',
            icon: 'folder',
            path: '/settlement/splitRulesManage'
          }
        ]
      },
      {
        code: 'SETTLEMENT_SETTING',
        name: '结算设置',
        icon: 'folder',
        children: [
          {
            code: 'TAX_RATE_SETTING',
            name: '税率设置',
            icon: 'folder',
            path: '/settlement/taxRateSetting'
          },
          {
            code: 'BANK_ACCOUNT_SETTING',
            name: '银行账户设置',
            icon: 'folder',
            path: '/settlement/bankAccountSetting'
          }
        ]
      },
      {
        code: 'COMMISSION_SETTLEMENT',
        name: '佣金结算',
        icon: 'folder',
        path: '/settlement/commissionSettlement'
      },
      {
        code: 'WRITE_OFF_MANAGE',
        name: '核销管理',
        icon: 'folder',
        path: '/settlement/writeOffManage'
      }
    ]
  },
  {
    code: 'ACCOUNTING_MANAGE',
    name: '核算管理',
    icon: 'folder',
    children: [
      {
        code: 'ACCOUNTING_REQUEST',
        name: '核算请求',
        icon: 'folder',
        path: '/settlement/computedRequest'
      },
      {
        code: 'ACCOUNTING_RULE',
        name: '核算规则设置',
        icon: 'folder',
        path: '/settlement/computedRule'
      },
      {
        code: 'ACCOUNT_PERIOD',
        name: '账期设置',
        icon: 'folder',
        path: '/settlement/paymentDay'
      }
      // {
      //   code: 'mappingTable',
      //   name: '映射表设置',
      //   icon: 'folder',
      //   path:'/settlement/mappingTable'

      // }
    ]
  },

  // 系统设置
  {
    code: 'SYSTEM_CONFIG',
    name: '系统设置',
    icon: 'folder',
    children: [
      {
        code: 'USER_MANAGE',
        name: '用户管理',
        icon: 'folder',
        path: '/system/userManage'
      },
      {
        code: 'ROLE_MANAGE',
        name: '角色管理',
        icon: 'folder',
        path: '/system/roleManage'
      },
      {
        code: 'PROJECT_MANAGE',
        name: '项目管理',
        icon: 'folder',
        path: '/system/projectMasterData'
      },
      {
        code: 'ORG_LEGAL_MANAGE',
        name: '法人管理',
        icon: 'folder',
        path: '/system/organizationManage'
      },
      {
        code: 'EXAMINE_BODY_MANAGE',
        name: '核算主体管理',
        icon: 'folder',
        path: '/system/examineManage'
      },
      {
        code: 'MENU_MANAGE',
        name: '菜单管理',
        icon: 'folder',
        path: '/system/menuManage'
      },
      {
        code: 'LOOKUP',
        name: '数据字典',
        icon: 'folder',
        path: '/system/lookup'
      }
    ]
  },

  //租赁政策管理
  {
    code: 'LEASE_MANAGE',
    name: '租赁政策管理',
    icon: 'folder',
    children: [
      {
        code: 'LEASE_MAINTAIN',
        name: '租赁政策维护',
        icon: 'folder',
        path: '/lease/maintain'
      },
      {
        code: 'LEASE_CALCULATE',
        name: '租赁计算维护',
        icon: 'folder',
        path: '/lease/calculate'
      }
    ]
  }

];

export default function getMenuData() {
  return menuData;
}

// 获取平级的菜单
export function getFlatMenuData(menus = getMenuData()) {
  let keys = {};
  menus.forEach((item) => {
    if (item.children) {
      keys[item.path] = {...item};
      keys = {...keys, ...getFlatMenuData(item.children)};
    } else {
      keys[item.path] = {...item};
    }
  });
  return keys;
}

export function findMenuByRouter(path, menuData = getFlatMenuData()) {
  const menuKey = Object.keys(menuData).find((key) => pathToRegexp(path).test(key));
  if (menuKey == null) {
    if (path === '/') {
      return null;
    }
    const lastIdx = path.lastIndexOf('/');
    if (lastIdx < 0) {
      return null;
    }
    if (lastIdx === 0) {
      return findMenuByRouter('/', menuData);
    }
    // 如果没有，使用上一层的配置
    return findMenuByRouter(path.substr(0, lastIdx), menuData);
  }
  return menuData[menuKey];
}
