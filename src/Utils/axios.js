import axios from 'axios';
import {message, Modal} from 'antd';
import {removeAccessToken} from './Common';
import IndexStore from '../Stores/IndexStore';
import {getAccessToken, logout} from 'Utils/authority';
import {entryUnique, removeRequestWhenFinish} from './repeat';

const hostname = location.hostname;
const port = location.port;
const protocol = location.protocol;// 协议

const CancelToken = axios.CancelToken;
const config = process.env.CONFIG;

axios.defaults.baseURL = `${config.server}`;
axios.defaults.timeout = 1000 * 120;

axios.interceptors.request.use((config) => {
  const accessToken = getAccessToken();
  const newConfig = config;
  newConfig.headers['Content-Type'] = 'application/json';
  newConfig.headers.Accept = 'application/json';
  if (accessToken) {
    newConfig.headers.Authorization = accessToken;
    newConfig.cancelToken = new CancelToken((c) => {
      entryUnique(config, c);
    });
  } else if (config.url == '/auth/jwt/externalLogin') {
    // newConfig.headers.Authorization = 'Basic Y2xpZW50OnNlY3JldA==';
  } else {
    newConfig.cancelToken = new CancelToken(((c) => {
      c();
      message.error('登录超时，请重新登录');
      logout();
    }));
  }
  IndexStore.setLoadingIf(true);
  return newConfig;
});

// http response 拦截器
/*axios.interceptors.response.use((response) => {
  if (response.status === 204) {
    return Promise.resolve(response);
  }

  return Promise.resolve(response.data);
}, (error) => {
  const response = error.response;
  if (response) {
    const status = response.status;
    switch (status) {
      case 400:
        if (response.data.error_description === 'Bad credentials') {
          message.error('用户名或密码错误！');
        }
        return Promise.resolve('');
      case 401:
        message.error('授权失败');
        IndexStore.setAuthenticated(false);
        removeAccessToken();
        window.location.href = `${protocol}//${hostname}:${port}/#/login`;
        return Promise.resolve('');
      case 403:
        message.error('拒绝访问');
        return Promise.resolve('');
      case 404:
        message.error('请求地址出错');
        return Promise.resolve('');
      case 408:
        message.error('请求超时');
        return Promise.resolve('');
      case 500:
        message.error('服务器内部错误');
        return Promise.resolve('');
      case 501:
        message.error('服务未实现');
        return Promise.resolve('');
      case 502:
        message.error('网关错误');
        return Promise.resolve('');
      case 503:
        message.error('服务不可用');
        return Promise.resolve('');
      case 504:
        message.error('网关超时');
        return Promise.resolve('');
      case 505:
        message.error('HTTP版本不受支持');
        return Promise.resolve('');
      default:
        message.error('未知错误！');
        return Promise.resolve('');
    }
  } else if (!axios.isCancel(error) && error) {
    message.error(error.toString());
    return Promise.resolve('');
  }
  if (axios.isCancel(error)) { // 取消请求的情况下，终端Promise调用链
    return new Promise(() => { });
  } else {
    return Promise.reject(error);
  }
});*/

axios.interceptors.response.use((response) => {
  removeRequestWhenFinish(response.config);
  IndexStore.setLoadingIf(false);
  const {status, message} = response.data;
  // 统一错误处理
  if (['ERROR'].includes(status)) {
    message.error(message);
    throw response.data;
  } else if (['WARNING'].includes(status)) {
    message.warn(message);
  }
  if (response.config.url.includes('needHeader')) {
    return Promise.resolve({...response.data, header: response.headers});
  } else {
    return Promise.resolve(response.data);
  }
}, (error) => {
  IndexStore.setLoadingIf(false);
  const response = error.response;
  let msg = response.data.message;
  if (response) {
    if (response.status === 400) {
      if (response.data.error_description && response.data.error_description ===
          'Bad credentials') {
        msg = '用户名或密码错误';
      }
    } else if (response.status == 401) {
      msg = '授权失败';
      // } else if (response.status == 500) {
      //   msg = '系统内部错误，请等候管理员处理';
    }
  }
  const confirm = IndexStore.getConfirmFlag;
  if (confirm) {
    Modal.error({
      centered: true,
      content: msg,
      okText: '确认',
      onOk: () => {
        IndexStore.setConfirmFlag(false);
      }
    });
  } else {
    const status = response.status;
    if (status >= 500) {
      message.error(msg || '系统正在升级，请稍后再试');
    } else {
      message.error(msg);
    }
  }
  if (response) {
    const status = response.status;
    switch (status) {
      case 401:
        IndexStore.setAuthenticated(false);
        removeAccessToken();
        localStorage.removeItem('openKeys');
        localStorage.removeItem('latestOpenKey');
        localStorage.clear;
        window.location.href = `${protocol}//${hostname}:${port}/#/login`;
        break;
      default:
        break;
    }
  }
  return Promise.reject(error);
});

export default axios;
