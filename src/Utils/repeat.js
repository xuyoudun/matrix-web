import _ from 'lodash';

const pending = new Map();

function ensureSlash(pathname) {
  return _.first(pathname) == '/' ? pathname : `/${pathname}`;
}

function generateUUID(config) {
  if (config.data) {
    if (_.isObject(config.data)) {
      return ensureSlash(
          `${config.url.replace(config.baseURL, '')}[data=${JSON.stringify(
              config.data)}][method=${config.method}]`);
    }else {
      return ensureSlash(
          `${config.url.replace(config.baseURL, '')}[data=${
              config.data}][method=${config.method}]`);
    }
  }
  return ensureSlash(
      `${config.url.replace(config.baseURL, '')}[method=${config.method}]`);
}

export const removeRequestWhenFinish = (config) => {
  const uuid = generateUUID(config);
  pending.delete(uuid);
  //window.console.log([...pending.values()]);
};

export const entryUnique = (config, c) => {
  if (config.method === 'get') {
    return; //暂不处理get请求
  }
  const uuid = generateUUID(config);
  const pend = {
    uuid,
    cancel: c,
    date: (new Date()).getTime()
  };
  if (pending.has(uuid)) {
    const exists = pending.get(uuid);
    pending.delete(exists);
    exists.cancel();
    window.console.error('repeat request', exists.uuid);
  }
  pending.set(uuid, pend);
};
