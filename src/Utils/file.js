import axios from './axios';
import {message} from 'antd';

const DEFAULT_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8';

export function download(res, filename, type = DEFAULT_TYPE) {
  //这里res.data是返回的blob对象
  let blob = new Blob([res], {type});
  if ('msSaveOrOpenBlob' in navigator) {
    // 兼容ie
    window.navigator.msSaveOrOpenBlob(blob, filename);
  } else {
    let downloadElement = document.createElement('a');
    let href = window.URL.createObjectURL(blob); //创建下载的链接
    downloadElement.href = href;
    downloadElement.download = filename; //下载后文件名
    document.body.appendChild(downloadElement);
    downloadElement.click(); //点击下载
    document.body.removeChild(downloadElement); //下载完成移除元素
    window.URL.revokeObjectURL(href); //释放掉blob对象
  }
}

export function axiosExport(url, filename = '下载文件', responseType = 'blob', filtype) {
  return new Promise((resolve, reject) => {
    axios({
      url: url,
      responseType,
      method: 'GET'
    }).then((exportResp) => {
      if (exportResp) {
        download(exportResp, `${filename}`, filtype);
        resolve('导出成功');
      }
    }).catch((err) => {
      const reader = new FileReader();
      reader.onloadend = (e) => {
        let errMsg = undefined;
        errMsg = JSON.parse(e.target.result);
        window.console.log(errMsg);
        const msg = errMsg && errMsg.message ? errMsg.message : '导出失败';
        message.error(msg);
      };
      reader.readAsText(err.response.data);
      reject(err);
    });
  });
}
