import Cookies from 'universal-cookie';

const cookies = new Cookies();
const DEFAULT_EXPIRES = 60 * 60 * 1;
const ACCESS_TOKEN = 'ACCESS_TOKEN';

export const setCookie = (name, value, option) => cookies.set(name, value, option);

export const removeCookie = (name, value, option) => cookies.remove(name, value, option);

// 前端存储cookie token
export function setAccessToken(token, expires = DEFAULT_EXPIRES) {
  const expirationDate = new Date(Date.now() + expires * 1000);
  setCookie(ACCESS_TOKEN, token, {
    path: '/',
    expires: expirationDate
  });
}

// 获取当前用户token
export function getAccessToken() {
  return cookies.get(ACCESS_TOKEN);
}

// 移除当前用户token
export function removeAccessToken() {
  removeCookie(ACCESS_TOKEN, {
    path: '/'
  });
}

// 刷新当前用户cookie
export function freshAccessToken() {
  const cookie = getAccessToken();
  if (cookie) {
    setAccessToken(cookie);
  }
}

// 退出登陆
export function logout() {
  removeAccessToken();
  //IndexStore.setUserInfo();
  localStorage.removeItem('openKeys');
  localStorage.removeItem('latestOpenKey');
  localStorage.clear;
  if (!window.location.href.includes('#/login')) {
    if (localStorage.getItem('logoutUrl')) {
      const url = localStorage.getItem('logoutUrl');
      localStorage.removeItem('logoutUrl');
      window.parent.location.href = url;
    } else {
      window.location.href = `${location.protocol}//${location.hostname}:${location.port}/#/login`;
    }
  }
}
