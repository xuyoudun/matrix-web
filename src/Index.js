import React from 'react';
import {render} from 'react-dom';
import Entry from './Entry';

render(
    <Entry/>,
    document.getElementById('app')
);
