import React, {Component} from 'react';
import 'moment/locale/zh-cn';
//import enUS from 'antd/es/locale/en_US';
import zhCN from 'antd/es/locale/zh_CN';
import {ConfigProvider, message} from 'antd';
import 'Styles/Entry.scss';
import {HashRouter, Route, Switch} from 'react-router-dom';
import {observer, Provider} from 'mobx-react';
// import LoginView from './Views/LoginView/LoginView';
import MainView from './Views/MainView/MainView';

message.config({
  duration: 5,
  maxCount: 1
});

@observer
class Entry extends Component {

  componentDidMount() {

  }

  render() {
    return (
        <ConfigProvider locale={zhCN}>
          <Provider>
            <HashRouter>
              <Switch>
                {/*<Route component={LoginView}
                                exact
                                path="/login"
                            />*/}
                <Route
                    path="/"
                    render={(props) => <MainView {...props} />}
                />
              </Switch>
            </HashRouter>
          </Provider>
        </ConfigProvider>
    );
  }
}

export default Entry;
